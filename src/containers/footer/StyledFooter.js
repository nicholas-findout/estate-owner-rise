import styled from 'styled-components';

const StyledFooter = styled.div`
    grid-area: footer;
    min-height: 100px;
`;

export default StyledFooter;