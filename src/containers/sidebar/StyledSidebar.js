import styled from 'styled-components';

const StyledSidebar = styled.div`
    grid-area: sidebar;
    min-height: 300px;
    max-width: 200px;
    border-top: 5px purple solid;
    background-color: rgba(0,0,0, 0.02);

    ul {
        list-style: none;
        padding: 0;
        margin: 0;
        text-align: center;

        li {
            border-bottom: 2px solid rgba(0, 0, 0, 0.1);
            
            @media screen and (pointer: fine) {
                :hover {
                    background-color: rgba(0, 0, 0, 0.05);
                }
            }

            a {
                height: 100%;
                color: inherit;
                text-decoration: none;

                div {
                    height: 100%;

                    h2 {
                        margin: 0;
                        padding: 20px 0 20px 0;
                    }
                }

                &.is-active {
                    div {
                        background-color: rgba(0, 255, 0, 0.1);
                    }
                }
            }
        }
    }
`;

export default StyledSidebar;