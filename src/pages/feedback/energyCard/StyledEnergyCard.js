import styled from 'styled-components';

const StyledEnergyCard = styled.div`
    display: flex;
    justify-content: space-around;
    align-items: baseline;
    padding: 20px;
`;

export default StyledEnergyCard;