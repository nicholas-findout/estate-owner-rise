import styled from 'styled-components';

const StyledHome = styled.div`
    display: flex;
    justify-content: center;
    align-items: center;
    flex-wrap: wrap;
`;

export default StyledHome;