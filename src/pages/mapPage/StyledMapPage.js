import styled from 'styled-components';

const StyledMapPage = styled.div`
    width: 100%;
    display: flex;
`;

export default StyledMapPage;